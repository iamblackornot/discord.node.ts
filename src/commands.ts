import { SlashCommandBuilder } from 'discord.js';
import { SERVICE } from './services';

const opt = new SlashCommandBuilder()
	.setName('opt')
	.setDescription('Set up stream notifications')
    .addSubcommand(subcommand =>
		subcommand
			.setName('in')
			.setDescription('Add stream to watchlist')
			//.addUserOption(option => option.setName('target').setDescription('The user')))
            .addNumberOption(option =>
                option.setName('service')
                .setDescription('Choose stream service from the list')
                .setRequired(true)
                .addChoices(
                    { name: 'twitch', value: SERVICE.TWITCH },
                    { name: 'wasd', value: SERVICE.WASD },
                )
            )
            .addStringOption(option =>
                option.setName('stream_id')
                .setDescription('Stream ID/Nickname')
                .setRequired(true)
            )
    )
    .addSubcommand(subcommand =>
		subcommand
			.setName('out')
			.setDescription('Remove stream from watchlist')
			//.addUserOption(option => option.setName('target').setDescription('The user')))
            .addNumberOption(option =>
                option.setName('service')
                .setDescription('Choose stream service from the list')
                .setRequired(true)
                .addChoices(
                    { name: 'twitch', value: SERVICE.TWITCH },
                    { name: 'wasd', value: SERVICE.WASD },
                )
            )
            .addStringOption(option =>
                option.setName('stream_id')
                .setDescription('Stream ID/Nickname')
                .setRequired(true)
            )
    )

const wathclist = new SlashCommandBuilder()
	.setName('watchlist')
	.setDescription('Show the list of tracked streams');

const test = new SlashCommandBuilder()
.setName('test')
.setDescription('Check if Uchitel ne v pomoike')

export default new Array(opt, wathclist, test);