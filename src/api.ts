import { SERVICE } from "./services";
import axios from "axios";
import { BotConfig } from "./config";
import Stream from "./stream";
import Result from "./result";

//type ServiceDelegate = (input: string) => void;

export default class APIDataProvider
{
    private twitchOAuthToken?: string;
    private serviceDelegates: Map<SERVICE, Function>;


    constructor() {
        this.serviceDelegates = new Map<SERVICE, Function>();
    }

    public async init() : Promise<boolean>
    {
        const res = await this.updateTwitchToken();

        if(res)
        {
            this.serviceDelegates = new Map<SERVICE, Function>();
            this.serviceDelegates.set(SERVICE.TWITCH, 
                (streams: Array<Stream>) => this.getTwitchStreamData(this.twitchOAuthToken!, streams));
            this.serviceDelegates.set(SERVICE.WASD, this.getWasdStreamData);
        }

        return res;
    }

    public async getStreamData(service: SERVICE, streams: Array<Stream>): Promise<Result>
    {
        return this.serviceDelegates.get(service)!(streams);
    }

    private async updateTwitchToken() : Promise<boolean>
    {
        let payload = {
            client_id: BotConfig.CLIENTID,
            client_secret: BotConfig.CLIENTSECRET,
            grant_type: "client_credentials"
        } 
        
        try
        {
            const response = await axios.post(BotConfig.TWITCH_OAUTH_TOKEN_URL, null, { params: payload })

            if(response?.status == 200)
            {
                this.twitchOAuthToken = response.data.access_token;
                console.log('got new twitch token');
                //console.log("OAuthToken = " + this.twitchOAuthToken);
                return true;
            }
        }
        catch(error)
        {
            console.log('[updateTwitchToken] ' + error);
        }

        console.log('[updateTwitchToken] smth fucked up');
        return false;
    }

    private async getWasdStreamData (streams: Array<Stream>) : Promise<Result>
    {
        return new Result(true, new Array<Stream>());
    }

    private async getTwitchStreamData (twitchOAuthToken: string, streams: Array<Stream>,) : Promise<Result>
    {
        if(streams.length === 0) 
            return new Result(true, new Array<Stream>());

        let userLogins = new Array<string>();

        for(const stream of streams)
        {
            userLogins.push(stream.id);
        }
    
        try
        {
            const res = await axios.get(BotConfig.TWITCH_API_URL,
            {
                headers: 
                {
                    "Client-ID": BotConfig.CLIENTID,
                    "Authorization": "Bearer " + this.twitchOAuthToken,
                },
                params:
                {
                    user_login: userLogins,
                }
            });

            if(res.status !== 200)
            {
                return new Result(false, null, `[getTwitchStreamData] response status = ${res.status}`);
            }

            let streams = res.data["data"];

            if(!streams)
            {
                return new Result(false, null, `[getTwitchStreamData] couldnt parse the response data`);
            }
    
            let result = new Array<Stream>();

            for(const entry of streams)
            {
                result.push(new Stream(SERVICE.TWITCH, entry['user_name'].toLowerCase(), entry['type'] == 'live'));
            }

            // let result = new Array<string>();

            // for(const entry of streams)
            // {
            //     result.push(entry['user_name'].toLowerCase());
            // }

            return new Result(true, result);
        }
        catch(error)
        { 
            if(error instanceof TwitchError)   
            {
                if(error?.response?.data?.status === 401)
                {
                    this.updateTwitchToken();

                    return new Result(false, null, "twitch token has expired");
                }

                return new Result(false, null, `[getTwitchStreamData] ${error?.response?.data}`);
            }
            else if(error instanceof Error)
            {
                new Result(false, null, `[getTwitchStreamData] ${error.message}`);
            }

            console.log(error);

            return new Result(false, null, `[getTwitchStreamData] smth fcked up`);
        }
    }
}

class TwitchError
{
    response: any;
}