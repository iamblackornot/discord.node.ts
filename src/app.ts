import { SERVICE } from './services';
import Watchlist from "./watchlist";
import { Client as DiscordClient, ClientOptions, Interaction, EmbedBuilder, TextChannel } from 'discord.js';
import { BotConfig } from "./config";
import { SetInteractionHandler, SubEvents } from "./events";
import { REST } from "@discordjs/rest"
import { Routes } from 'discord-api-types/v10';
import commands from "./commands";
import handleInteraction from "./interactions";
import Stream from "./stream";

class Application
{
    private client?: DiscordClient;
    private watchlist: Watchlist;
    private channel?: TextChannel;

    constructor()
    {
        this.watchlist = new Watchlist();
    }

    public async start()
    {  
        await this.initClient();

        const res = await this.watchlist!.init(this.channel!);

        if(!res.success)
        {
            console.log("failed to init watchlist, reason: ", res.errorMessage);
            return;
        }

        console.log("everything is ok, started");
        //await this.registerCommands();
    }
    private async registerCommands()
    {
        const rest = new REST({ version: '10' }).setToken(BotConfig.TOKEN);
        await rest.put(
            Routes.applicationCommands(BotConfig.DISCORD_APP_ID),
            { body: commands },
        );
        
    }
    private async initClient(): Promise<void>
    {
        const options: ClientOptions = 
        {
            intents: [],
        }

        this.client = new DiscordClient(options);

        SubEvents(this.client);
        SetInteractionHandler(this.client,this.watchlist, handleInteraction);

        await this.client.login(BotConfig.TOKEN);

        this.channel = await this.client.channels.fetch(BotConfig.PODPOLKA) as TextChannel;
    }
}

const app: Application = new Application();
app.start();

