import { Client as DiscordClient, Interaction } from "discord.js";
import Watchlist from "./watchlist";

export function SubEvents (client: DiscordClient): void 
{
    client.on("ready", async () => 
    {
        if (!client.user || !client.application) {
            return;
        }

        console.log(`${client.user.username} is online`);
    });

    client.on("disconnect", async () => 
    {
        console.log("uchitel otletel v pomoyku");
    });

    client.on("error", async (err) => 
    {
        console.log(err.message)
    });
};

export function SetInteractionHandler(client: DiscordClient, watchlist: Watchlist, 
    func: (interaction: Interaction, watchlist: Watchlist) => void): void
{
    client.on('interactionCreate', interaction => {
        func(interaction, watchlist);
    });
}