import * as dotenv from "dotenv";

dotenv.config({ path: __dirname + '/.env' });

interface IConfigOptions
{
    TWITCH_API_URL: string,
    TWITCH_OAUTH_TOKEN_URL: string,

    REBENOKTEST: string,
    PODPOLKA: string,

    DISCORD_APP_ID: string,

    TOKEN: string,
    CLIENTID: string,
    CLIENTSECRET: string,

    MYSQL_DATABASE_URL: string,
}

export type ConfigOptions = Readonly<IConfigOptions>;

export const BotConfig: ConfigOptions = 
{
    TWITCH_API_URL: process.env.TWITCH_API_URL!,
    TWITCH_OAUTH_TOKEN_URL: process.env.TWITCH_OAUTH_TOKEN_URL!,

    REBENOKTEST: process.env.REBENOKTEST!,
    PODPOLKA: process.env.PODPOLKA!,

    DISCORD_APP_ID: process.env.DISCORD_APP_ID!,

    TOKEN: process.env.TOKEN!,
    CLIENTID: process.env.CLIENTID!,
    CLIENTSECRET: process.env.CLIENTSECRET!,

    MYSQL_DATABASE_URL: process.env.MYSQL_DATABASE_URL!,
}