import { TextChannel, EmbedBuilder } from 'discord.js';
import APIDataProvider from "./api";
import ISreamListProvider from './database';
import { MySQLAdapter } from "./database";
import Result from "./result";
import { SERVICE, ServiceToHostname } from "./services";
import Stream from "./stream";


export default class Watchlist 
{
    private dbAdapter: ISreamListProvider;
    private apiDataProvider: APIDataProvider;

    private streamsById: Map<string, Stream>;
    private streamsByService: Map<SERVICE, Array<Stream>>;

    private channel?: TextChannel;
    private updateInterval: number = 10 * 1000;
    private timeout: number = 10 * 60 * 1000;

    constructor()
    {
        this.streamsById = new Map<string, Stream>();
        this.streamsByService = new Map<SERVICE, Array<Stream>>();

        for(const service of ServiceToHostname.keys())
        {
            this.streamsByService.set(service, new Array<Stream>());
        }

        this.apiDataProvider = new APIDataProvider();
        this.dbAdapter = new MySQLAdapter();
    }

    public async addStream(stream: Stream) : Promise<Result>
    {
        const res = await this.dbAdapter.addStream(stream.service, stream.id);

        if(res.success)
        {
            this.addStreamToInnerLists(stream);
        }

        return res;
        // console.log(this.streamsById);
        // console.log(this.streamsByService);
    }

    public async removeStream(service: SERVICE, id: string) : Promise<Result>
    {
        const res = await this.dbAdapter.removeStream(service, id);

        if(res.success)
        {
            this.streamsById.delete(id);
        
            let arr = this.streamsByService.get(service)!;
            const index = arr.findIndex((stream: Stream) => stream.id == id);
            arr.splice(index, 1);
        }

        return res;
        // console.log(this.streamsById);
        // console.log(this.streamsByService);
    }

    public async getAllStreams() : Promise<Result>
    {
        return new Result(true, this.streamsById.values());
    }

    public async init(channel: TextChannel) : Promise<Result>
    {
        this.channel = channel;

        const dbRes = await this.dbAdapter.getAllStreams();

        if(!dbRes.success)
        {
            return dbRes;
        }

        this.addStreamRangeToInnerLists(dbRes.data as Array<Stream>);

        console.log(`[Watchlist init] loaded streams from db`);

        const initRes = await this.apiDataProvider.init();
        
        if(!initRes)
        {
            return new Result(false, null, "failed to init APIDataProvider");
        }

        console.log(`[Watchlist init] initialized APIDataProvider`);

        // console.log(this.streamsById);
        // console.log(this.streamsByService);

        const updateRes = await this.updateStreamData(true);

        if(!updateRes.success)
        {
            return updateRes;
        }

        console.log(`[Watchlist init] updated stream data`);

        setInterval(this.updateLoop.bind(this), this.updateInterval);

        return new Result(true, null);
    }

    private async updateLoop()
    {
        const res = await this.updateStreamData();

        if(!res.success)
        {
            console.log(`[updateLoop] failed to update stream data, reason: ${res.errorMessage}`);
        }
    }
    private async updateStreamData(init: boolean = false) : Promise<Result>
    {
        const services = ServiceToHostname.keys();

        for(const service of services)
        {
            const res = await this.apiDataProvider.getStreamData(
                service, this.streamsByService.get(service)!);

            if(!res.success)
            {
                if(init)
                {
                    return new Result(false, null, 
                        `[updateStreamData] initial stream data 
                        update failed [${SERVICE[service]}]`);
                }
                else
                {
                    if(!res.success) continue;
                }
            }

            const streamData = res.data as Array<Stream>;
            let liveStreams = new Array<string>();

            for(const entry of streamData)
            {
                liveStreams.push(entry.id);
            }

            for(const stream of this.streamsByService.get(service)!)
            {
                if(stream.online && 
                    !liveStreams.includes(stream.id))
                {
                    if(!init)
                    {
                        console.log(`${stream.id} is offline`);
                    }
                    this.streamsById.get(stream.id)!.online = false;
                    this.streamsById.get(stream.id)!.lastOnline = Date.now();
                }
                else if(!stream.online && 
                        liveStreams.includes(stream.id))
                {
                    if(!init)
                    {
                        console.log(`${stream.id} is online`);
                        
                        if((Date.now() - stream.lastOnline) > this.timeout)
                        {
                            this.notificate(stream);
                        }
                    }

                    this.streamsById.get(stream.id)!.online = true;
                }
            }
        }

        return new Result(true, null);
    }

    private addStreamToInnerLists(stream: Stream) : void
    {
        this.streamsById.set(stream.id, stream);
        this.streamsByService.get(stream.service)!.push(stream);
    }

    private addStreamRangeToInnerLists(streams: Array<Stream>) : void
    {
        for(const stream of streams)
        {
            this.addStreamToInnerLists(stream);
        }
    }

    private notificate(stream: Stream)
    {
        let embed = new EmbedBuilder()
            .setColor('#0099ff')
            .setTitle(`${stream.id} went live`)
            .setDescription(`[**${SERVICE[stream.service].toUpperCase()}**]` +
                            `(${ServiceToHostname.get(stream.service)}${stream.id})`);        

        this.channel?.send({ embeds: [ embed ] });
    }
}