import { BotConfig } from './config';
import { Connection, createConnection, createPool, Pool } from 'mysql'
import parseDatabaseUrl from 'ts-parse-database-url';
import Result from './result';
import { SERVICE } from './services';
import Stream from './stream';

export default interface ISreamListProvider
{
    addStream(service: SERVICE, id: string) : Promise<Result>;
    removeStream(service: SERVICE, id: string) : Promise<Result>;
    getAllStreams() : Promise<Result>;
}
export class MockDataProvider implements ISreamListProvider
{
    public async addStream(service: SERVICE, id: string) : Promise<Result>
    {
        return new Result(true, null);
    }
    public async removeStream(service: SERVICE, id: string) : Promise<Result>
    {
        return new Result(true, null);
    }
    public async getAllStreams() : Promise<Result>
    {
        let streams = new Array<Stream>();
        streams.push(new Stream(SERVICE.TWITCH, "iamblackornot"));

        return new Result(true, streams);
    }
}
export class MySQLAdapter implements ISreamListProvider
{
    private connection?: Connection;
    //private pool: Pool;

    constructor()
    {     
        // this.pool = createPool(
        //     {
        //         connectionLimit: 3,
        //         ...parseDatabaseUrl(BotConfig.MYSQL_DATABASE_URL),
        //     }
        // )
        //this.test();
        //this.testInsert();
    }

    public async addStream(service: SERVICE, id: string) : Promise<Result>
    {
        return await this.query(
            "INSERT INTO streams (service_id, stream_id) VALUES(?, ?)",
            [service, id]);
    }

    public async removeStream(service: SERVICE, id: string) : Promise<Result>
    {
        let res = await this.query(
            "DELETE FROM streams WHERE service_id = ? AND stream_id = ?",
            [service, id]);

        if(!res.success)
        {
            return res;
        } 
        else 
        {
            if(res.data?.affectedRows == 0)
            {
                res.success = false;
                res.errorMessage = "there is no such stream in the watchlist";
                return res;
            }

            return res;
        }
    }

    public async getAllStreams() : Promise<Result>
    {
        let res = await this.query(
            "SELECT * FROM streams", null);

        if(!res.success)
        {
            res.data = null;
            return res;
        }

        let streams = new Array<Stream>();
        
        if(res.data?.length > 0)
        {
            for(const row of res.data)
            {
                streams.push(new Stream(row?.service_id, row?.stream_id));
            }
        }

        res.data = streams;
        return res;
    }

    private async testSelect()
    {
        var res = await this.query("SELECT * FROM streams", null);
        console.log("content-length = ", res.data.length);
        console.log("row[0] = ", res.data[0]);

        res = await this.query("SELECT * FROM streams", null);
        console.log("content-length = ", res.data.length);
        console.log("row[1] = ", res.data[1]);
    }

    private async testInsert()
    {
        var res = await this.query(
            "INSERT INTO streams (service_id, stream_id) VALUES(?, ?)",
            [1, 'amouranth']);

        if(!res.success)
        {
            console.log(res.errorMessage);
        }
    }

    private async query(sqlStatement: string, ...args: any) : Promise<Result> {
        return await new Promise<Result>((resolve, reject) => 
        {
            this.connection = createConnection(parseDatabaseUrl(BotConfig.MYSQL_DATABASE_URL));
            this.connection?.query(sqlStatement, ...args, (err: any, res: any) => {
            //this.pool.query(sqlStatement, ...args, (err: any, res: any) => {
                //this.pool.end();
                this.connection?.end(() =>
                {
                    if(err) 
                    {
                        let message: string;

                        switch(err.code)
                        {
                            case 'ER_DUP_ENTRY': 
                                message = "ERR: This stream is already in the watchlist";
                                break;
                            default:
                                message = "ERR: MySQL database query failed";
                                console.log("MySQL database query failed, reason: ", err);
                        }

                        resolve(new Result(false, null, message));
                    }
                    else 
                    {
                        resolve(new Result(true, res))
                    }
                });
            })
        })
    }
}