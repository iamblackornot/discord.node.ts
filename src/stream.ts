import { TimeLike } from "fs";
import { SERVICE } from "./services";

export default class Stream
{
    public readonly service: SERVICE;
    public readonly id: string;
    public online: boolean;
    public lastOnline: number;

    constructor(service: SERVICE, id: string, online: boolean = false) 
    {
        this.service = service
        this.id = id
        this.online = online
        this.lastOnline = 0;
    }
}