export default class Result
{
    success : boolean;
    data : any;
    errorMessage: string;

    constructor(success : boolean, data : any, errorMessage : string = "")
    {
        this.success = success;
        this.data = data;
        this.errorMessage = errorMessage;
    }
}