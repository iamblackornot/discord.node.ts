import { SERVICE, ServiceToHostname } from './services';
import { Interaction, EmbedBuilder, TextChannel } from 'discord.js';
import Stream from './stream';
import Watchlist from './watchlist';

export default async function handleInteraction(interaction: Interaction, watchlist: Watchlist)
{
    if (!interaction.isChatInputCommand()) return;

    // console.log(interaction);
    // console.log(interaction?.commandName);

    if(interaction?.commandName == 'opt')
    {
        //await interaction.deferReply();
        //console.log("opt command branch");

        const service: SERVICE = interaction?.options?.get('service')?.value as SERVICE;
        const id: string = interaction?.options?.get('stream_id')?.value as string;

        //console.log(`service = ${service}, id = ${id}`);

        if(!id || id.length < 4 || id.length > 25)
        {
            interaction.reply('ERR: Stream ID must be 4-25 characters long!');
            return;
        }

        if(interaction?.options?.getSubcommand() == 'in')
        {
            const res = await watchlist.addStream(new Stream(service, id));

            if(!res.success)
            {
                interaction.reply(res.errorMessage);
                return;
            }

            interaction.reply(`${SERVICE[service].toLowerCase()}:**${id}** was added to the watchlist`);
            return;
        }
        else if(interaction.options.getSubcommand() == 'out')
        {
            const res = await watchlist.removeStream(service, id);

            if(!res.success)
            {
                interaction.reply(res.errorMessage);
                return;
            }
            console.log(res.data);
            interaction.reply(`${SERVICE[service].toLowerCase()}:**${id}** was removed from the watchlist`);
            return;
        }
    }
    else if(interaction?.commandName == 'watchlist')
    {
        let embed = new EmbedBuilder()
            .setColor('#0099ff')
            .setTitle('Watchlist')           

        const res = await watchlist.getAllStreams();

        if(!res.success)
        {
            embed.setDescription(res.errorMessage);
        }
        else if(res.data?.length == 0)
        {
            embed.setDescription("**EMPTY**");
        }
        else
        {
            let message: string = "";
            const streams: Array<Stream> = res.data as Array<Stream>;

            for(const stream of streams)
            {
                message +=  `\n` +
                            `[**${SERVICE[stream.service].toUpperCase()}**]` +
                            `(${ServiceToHostname.get(stream.service)}${stream.id}) ` + 
                            `**${stream.id.toUpperCase()}**`;
            }

            embed.setDescription(message);
        } 

        interaction.reply({ embeds: [ embed ] });
        return;
    }
    else if(interaction?.commandName == 'test')
    {
        interaction.reply('loh');
        return;
    }
    else
    {
        interaction.reply('unknown command');
    }
}