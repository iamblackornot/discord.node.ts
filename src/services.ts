export enum SERVICE
{
    TWITCH = 1,
    WASD = 2,
}

let ServiceToHostnameMap: Map<SERVICE, string> = new Map<SERVICE, string>();
ServiceToHostnameMap.set(SERVICE.TWITCH, "https://www.twitch.tv/");
ServiceToHostnameMap.set(SERVICE.WASD, "https://wasd.tv");

export const ServiceToHostname = ServiceToHostnameMap;